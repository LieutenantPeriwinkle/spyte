use crate::cli::Args;
use anyhow::Result;
use directories::BaseDirs;
use serde::Deserialize;
use std::{
    fs::{create_dir_all, metadata, read_to_string, File},
    path::PathBuf,
};
use toml::from_str;

#[derive(Deserialize, Debug)]
#[serde(default)]
pub struct Config {
    pub target_addr: String,
    pub username: String,
    pub password: String,
    pub skin: String,
}

impl Config {
    pub fn open() -> Result<Self> {
        Ok(from_str(&read_to_string(setup_config_dir()?)?)?)
    }
    pub fn set_args(&mut self, args: Args) {
        if let Some(x) = args.target_addr {
            self.target_addr = x;
        }
        if let Some(x) = args.username {
            self.username = x;
        }
        if let Some(x) = args.password {
            self.password = x;
        }
        if let Some(x) = args.skin {
            self.skin = x;
        }
    }
}

impl Default for Config {
    fn default() -> Self {
        Self {
            target_addr: String::from("127.0.0.1:22"),
            username: String::from("retrospy"),
            password: String::from("retrospy"),
            skin: String::from("default"),
        }
    }
}

lazy_static::lazy_static! {
    pub static ref DIRS: BaseDirs = BaseDirs::new().unwrap();
}

fn setup_config_dir() -> Result<PathBuf> {
    let mut config_dir = DIRS.config_dir().to_owned();
    config_dir.push("spyte");
    config_dir.push("skins");
    create_dir_all(&config_dir)?;
    config_dir.pop();
    config_dir.push("config.toml");
    if metadata(&config_dir).is_err() {
        File::create(&config_dir)?;
    }
    Ok(config_dir)
}
