use crate::config::DIRS;
use crate::packet::{InputFrame, SwitchButton};
use anyhow::Result;
use crow::{Context, Texture};
use serde::{Deserialize, Serialize};
use std::{collections::HashMap, fs::read_to_string, path::PathBuf};
use toml::from_str;

#[derive(Serialize, Deserialize, Debug)]
pub struct Skin {
    background: String,
    buttons: Buttons,
    sticks: Sticks,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Buttons {
    a: Button,
    b: Button,
    x: Button,
    y: Button,
    plus: Button,
    minus: Button,
    home: Button,
    cap: Button,
    zl: Button,
    zr: Button,
    l: Button,
    r: Button,
    up: Button,
    down: Button,
    left: Button,
    right: Button,
    ls: Button,
    rs: Button,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Button {
    image: String,
    pos: Pos,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Sticks {
    left: Stick,
    right: Stick,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Stick {
    image: String,
    pos: Pos,
    range: u32,
}

#[derive(Serialize, Deserialize, Debug, Copy, Clone)]
pub struct Pos {
    x: i32,
    y: i32,
}

impl Into<(i32, i32)> for Pos {
    fn into(self) -> (i32, i32) {
        (self.x, self.y)
    }
}

impl Skin {
    pub fn open(name: &str) -> Result<(Self, PathBuf)> {
        let mut path = DIRS.config_dir().to_owned();
        let p: PathBuf = ["spyte", "skins", name, "skin.toml"].iter().collect();
        path = path.join(p);
        let s = from_str(&read_to_string(&path)?)?;
        path.pop();
        Ok((s, path))
    }
}

pub struct RenderSkin {
    pub background: Texture,
    buttons: HashMap<SwitchButton, TexButton>,
    pub lstick: TexStick,
    pub rstick: TexStick,
}

pub struct TexButton {
    pub image: Texture,
    pub pos: Pos,
}

impl TexButton {
    fn from_button(ctx: &mut Context, b: Button, base: &PathBuf) -> Result<Self> {
        Ok(Self {
            pos: b.pos,
            image: Texture::load(ctx, base.join(b.image))?,
        })
    }
}

pub struct TexStick {
    pub image: Texture,
    pub pos: Pos,
    pub range: u32,
}

impl TexStick {
    fn from_stick(ctx: &mut Context, s: Stick, base: &PathBuf) -> Result<Self> {
        Ok(Self {
            pos: s.pos,
            range: s.range,
            image: Texture::load(ctx, base.join(s.image))?,
        })
    }
}

impl RenderSkin {
    pub fn from_skin(ctx: &mut Context, s: Skin, base: &PathBuf) -> Result<Self> {
        use SwitchButton::*;
        use TexButton as B;
        use TexStick as S;
        let mut map = HashMap::new();
        map.insert(A, B::from_button(ctx, s.buttons.a, base)?);
        map.insert(B, B::from_button(ctx, s.buttons.b, base)?);
        map.insert(X, B::from_button(ctx, s.buttons.x, base)?);
        map.insert(Y, B::from_button(ctx, s.buttons.y, base)?);
        map.insert(Plus, B::from_button(ctx, s.buttons.plus, base)?);
        map.insert(Minus, B::from_button(ctx, s.buttons.minus, base)?);
        map.insert(Home, B::from_button(ctx, s.buttons.home, base)?);
        map.insert(Cap, B::from_button(ctx, s.buttons.cap, base)?);
        map.insert(Zl, B::from_button(ctx, s.buttons.zl, base)?);
        map.insert(Zr, B::from_button(ctx, s.buttons.zr, base)?);
        map.insert(L, B::from_button(ctx, s.buttons.l, base)?);
        map.insert(R, B::from_button(ctx, s.buttons.r, base)?);
        map.insert(Up, B::from_button(ctx, s.buttons.up, base)?);
        map.insert(Down, B::from_button(ctx, s.buttons.down, base)?);
        map.insert(Left, B::from_button(ctx, s.buttons.left, base)?);
        map.insert(Right, B::from_button(ctx, s.buttons.right, base)?);
        map.insert(Ls, B::from_button(ctx, s.buttons.ls, base)?);
        map.insert(Rs, B::from_button(ctx, s.buttons.rs, base)?);
        Ok(Self {
            background: Texture::load(ctx, base.join(s.background))?,
            buttons: map,
            lstick: S::from_stick(ctx, s.sticks.left, base)?,
            rstick: S::from_stick(ctx, s.sticks.right, base)?,
        })
    }
    pub fn size(&self) -> (u32, u32) {
        self.background.dimensions()
    }
    pub fn to_draw(&self, frame: &InputFrame) -> Vec<&TexButton> {
        let mut r = vec![];
        for k in &frame.buttons {
            //println!("{:?}", k);
            r.push(self.buttons.get(k).unwrap());
        }
        r
    }
}
